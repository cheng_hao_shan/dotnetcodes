﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DotNet.Utilities.YtSQLiteHelper;
using Newtonsoft.Json;

namespace DotNet.Utilities.Test
{
    public partial class FormTest : Form
    {
        public FormTest()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 枚举扩展测试
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnEnumExtension_Click(object sender, EventArgs e)
        {
            var errCode = ErrorCode.内存耗尽;
            TBInfo.Text += $"获取枚举描述：{errCode.GetEnumDescription()}\r\n";
        }

        /// <summary>
        /// SQLite数据库操作测试
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnYtSQLiteHelper_Click(object sender, EventArgs e)
        {
            SQLiteDbManager.SetDbDir(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory));

            TBInfo.Text += $"查询表中数据...\r\n";
            var dataList = SQLiteDbManager.Query<TestTable>("select * from TestTable");
            TBInfo.Text += $"查询表中数据结果：{JsonConvert.SerializeObject(dataList)}\r\n\r\n";

            TBInfo.Text += $"测试表中插入数据...\r\n";
            SQLiteDbManager.Insert(new TestTable() { Name = "哈哈哈" });
            TBInfo.Text += $"测试表中插入数据完成\r\n\r\n";

            TBInfo.Text += $"查询表中数据...\r\n";
            dataList = SQLiteDbManager.Query<TestTable>("select * from TestTable");
            TBInfo.Text += $"查询表中数据结果：{JsonConvert.SerializeObject(dataList)}\r\n\r\n";

            TBInfo.Text += $"测试表中删除数据...\r\n";
            SQLiteDbManager.Excute("delete from TestTable where id='2'");
            TBInfo.Text += $"测试表中插入数据完成\r\n\r\n";

            TBInfo.Text += $"查询表中数据...\r\n";
            dataList = SQLiteDbManager.Query<TestTable>("select * from TestTable");
            TBInfo.Text += $"查询表中数据结果：{JsonConvert.SerializeObject(dataList)}\r\n\r\n";
        }
    }

    #region EnumExtension

    public enum ErrorCode
    {
        [Description("Common_1000001"), ExceptionLevel(Level = 10)]
        内存耗尽,
        [Description("Common_1000002"), ExceptionLevel(Level = 10)]
        软件自动更新异常,
        [Description("Common_1000003"), ExceptionLevel(Level = 10)]
        软件初始化异常,
        [Description("Common_1000004"), ExceptionLevel(Level = 10)]
        软件系统异常,
    }

    [AttributeUsage(AttributeTargets.Field)]
    public class ExceptionLevelAttribute : Attribute
    {
        public int Level { get; set; } = 10;
    }

    #endregion
}
