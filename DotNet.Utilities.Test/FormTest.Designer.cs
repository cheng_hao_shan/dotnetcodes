﻿namespace DotNet.Utilities.Test
{
    partial class FormTest
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.TBInfo = new System.Windows.Forms.TextBox();
            this.BtnEnumExtension = new System.Windows.Forms.Button();
            this.BtnYtSQLiteHelper = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TBInfo
            // 
            this.TBInfo.Location = new System.Drawing.Point(13, 13);
            this.TBInfo.Multiline = true;
            this.TBInfo.Name = "TBInfo";
            this.TBInfo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.TBInfo.Size = new System.Drawing.Size(634, 417);
            this.TBInfo.TabIndex = 0;
            // 
            // BtnEnumExtension
            // 
            this.BtnEnumExtension.Location = new System.Drawing.Point(663, 13);
            this.BtnEnumExtension.Name = "BtnEnumExtension";
            this.BtnEnumExtension.Size = new System.Drawing.Size(108, 23);
            this.BtnEnumExtension.TabIndex = 1;
            this.BtnEnumExtension.Text = "EnumExtension";
            this.BtnEnumExtension.UseVisualStyleBackColor = true;
            this.BtnEnumExtension.Click += new System.EventHandler(this.BtnEnumExtension_Click);
            // 
            // BtnYtSQLiteHelper
            // 
            this.BtnYtSQLiteHelper.Location = new System.Drawing.Point(663, 42);
            this.BtnYtSQLiteHelper.Name = "BtnYtSQLiteHelper";
            this.BtnYtSQLiteHelper.Size = new System.Drawing.Size(108, 23);
            this.BtnYtSQLiteHelper.TabIndex = 2;
            this.BtnYtSQLiteHelper.Text = "YtSQLiteHelper";
            this.BtnYtSQLiteHelper.UseVisualStyleBackColor = true;
            this.BtnYtSQLiteHelper.Click += new System.EventHandler(this.BtnYtSQLiteHelper_Click);
            // 
            // FormTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 442);
            this.Controls.Add(this.BtnYtSQLiteHelper);
            this.Controls.Add(this.BtnEnumExtension);
            this.Controls.Add(this.TBInfo);
            this.Name = "FormTest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormTest";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TBInfo;
        private System.Windows.Forms.Button BtnEnumExtension;
        private System.Windows.Forms.Button BtnYtSQLiteHelper;
    }
}

