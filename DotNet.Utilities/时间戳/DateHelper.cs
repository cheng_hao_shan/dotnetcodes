﻿/* 
 * 源码己托管: http://gitee.com/dlgcy/dotnetcodes
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DLGCY.Utilities
{
    public class DateHelper
    {
        /// <summary>
        /// dlgcy:判断传入的日期是否是当天或之后;
        /// </summary>
        /// <param name="compareDayStr">要比较的日期字符串（如2015-05-06）</param>
        /// <returns>是否</returns>
        public static bool TodayOrLater(string compareDayStr)
        {
            string todayStr = DateTime.Now.ToString("yyyy-MM-dd");
            DateTime today = Convert.ToDateTime(todayStr);
            DateTime compareDay = Convert.ToDateTime(compareDayStr);

            int compare = DateTime.Compare(compareDay, today);
            if (compare >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// dlgcy:将数值月份转为英文简写的形式;
        /// </summary>
        public static string NumberMonthToEnShort(int month)
        {
            string[] shortEn = new[]
            {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

            if (month > 0 && month <= 12)
            {
                return shortEn[month - 1];
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// dlgcy:将小数形式的时间转为带冒号的显示时间格式;
        /// </summary>
        public static string ChangeDecimalTimeToShowTime(string decimalTime)
        {
            string showTime;
            if (decimalTime.Contains("."))
            {
                string[] leftAndRight = decimalTime.Split('.');
                string left = leftAndRight[0];
                string right = leftAndRight[1];

                if (string.IsNullOrWhiteSpace(right))
                {
                    right = "00";
                }
                else
                {
                    double rightValue = double.Parse("0." + right);
                    // 乘60是为了转为60进制，除以100是为了保证前面都是“0.”，加0是为了截取子串时保证有足够的位数（加一个0应该就够了）; 
                    right = ((rightValue * 60 / 100) + "00").Substring(2, 2);
                }

                showTime = left + ":" + right;
            }
            else
            {
                showTime = decimalTime + ":00";
            }

            return showTime;
        }

        /// <summary>
        /// 获取当日的剩余/已过秒数
        /// </summary>
        /// <param name="isRest">是否获取剩余秒数</param>
        /// <returns>秒数</returns>
        public static double GetTodaySecond(bool isRest = true)
        {
            if (isRest)
            {
                DateTime dtime = DateTime.Today.AddDays(1).Date;
                TimeSpan ts = dtime - DateTime.Now;
                return ts.TotalSeconds;
            }
            else
            {
                DateTime dtime = DateTime.Today.Date;
                TimeSpan ts = DateTime.Now - dtime;
                return ts.TotalSeconds;
            }
        }
    }
}
