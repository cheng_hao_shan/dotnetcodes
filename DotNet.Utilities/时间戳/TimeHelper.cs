/*
 *源码己托管:
 * http://git.oschina.net/kuiyu/dotnetcodes
 * http://gitee.com/dlgcy/dotnetcodes
 */
using System;

namespace DotNet.Utilities
{
    public class TimeHelper
    {
        /// <summary>
        /// 时间戳转为C#格式时间
        /// </summary>
        /// <returns></returns>
        public static DateTime GetTimeByJavascript(long jsTimeStamp)
        {
            DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1)); // 当地时区
            DateTime dt = startTime.AddMilliseconds(jsTimeStamp);
            return dt;
        }

        /// <summary>
        /// 把时间转换为javascript所使用的时间
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static long ConvertDateTimeByJavascript(DateTime dt)
        {
            DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1)); // 当地时区
            long timeStamp = (long)(dt - startTime).TotalMilliseconds; // 相差毫秒数
            return timeStamp;
        }

        /// <summary>
        /// 把时间格式转换为Unix时间戳格式
        /// </summary>
        /// <returns></returns>
        public static long ConvertDateTimeByUnix(DateTime time)
        {
            DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1)); // 当地时区
            long timeStamp = (long)(time - startTime).TotalSeconds; // 相差秒数
            return timeStamp;
        }

        /// <summary>
        /// 获取时间戳;
        /// </summary>
        /// <returns></returns>
        public static long GetTimeStamp()
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds);
        }

        /// <summary>
        /// 把Unix时间戳转化为时间
        /// </summary>
        /// <param name="unixTimeStamp"></param>
        /// <returns></returns>
        public static DateTime GetTimeByUnix(long unixTimeStamp)
        {
            DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1)); // 当地时区
            DateTime dt = startTime.AddSeconds(unixTimeStamp);
            return dt;
        }

        /// <summary>
        /// 把Unix毫秒时间戳转化为时间
        /// </summary>
        /// <param name="unixTimeStamp"></param>
        /// <returns></returns>
        public static DateTime GetTimeByUnixMillis(long unixTimeStamp)
        {
            DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1)); // 当地时区
            return startTime.AddSeconds(unixTimeStamp / 1000.0);
        }

        /// <summary>
        /// 获取以0点0分0秒开始的日期
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime GetStartDateTime(DateTime dateTime)
        {
            if (dateTime.Hour != 0)
            {
                var year = dateTime.Year;
                var month = dateTime.Month;
                var day = dateTime.Day;
                var hour = "0";
                var minute = "0";
                var second = "0";
                dateTime = Convert.ToDateTime($"{year}-{month}-{day} {hour}:{minute}:{second}");
            }
            return dateTime;
        }

        /// <summary>
        /// 获取以23点59分59秒结束的日期
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime GetEndDateTime(DateTime dateTime)
        {
            if (dateTime.Hour != 23)
            {
                var year = dateTime.Year;
                var month = dateTime.Month;
                var day = dateTime.Day;
                var hour = "23";
                var minute = "59";
                var second = "59";
                dateTime = Convert.ToDateTime($"{year}-{month}-{day} {hour}:{minute}:{second}");
            }
            return dateTime;
        }
    }
}
