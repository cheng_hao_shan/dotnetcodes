﻿using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
/*
 * 使用 Gma.QrCodeNet.Core 包
 */
namespace DotNet.Utilities
{
    public class QrCodeGma
    {
        /// <summary>
        /// 创建二维码为 Image
        /// </summary>
        /// <param name="content">二维码内容（一般为链接地址）</param>
        /// <returns>Image</returns>
        public static Image BuildQrCodeAsImage(string content)
        {
            if (string.IsNullOrEmpty(content))
            {
                return null;
            }

            QrEncoder qrEncoder = new QrEncoder(ErrorCorrectionLevel.M);
            qrEncoder.TryEncode(content, out QrCode qrCode);

            GraphicsRenderer renderer = new GraphicsRenderer(new FixedModuleSize(5, QuietZoneModules.Two), Brushes.Black, Brushes.White);
            using (MemoryStream ms = new MemoryStream())
            {
                renderer.WriteToStream(qrCode.Matrix, ImageFormat.Bmp, ms);
                return Image.FromStream(ms);
            }
        }

        /// <summary>
        /// 创建二维码为 byte[]
        /// </summary>
        /// <param name="content">二维码内容（一般为链接地址）</param>
        /// <returns>byte[]</returns>
        public static byte[] BuildQrCodeAsByteArray(string content)
        {
            if (string.IsNullOrEmpty(content))
            {
                return null;
            }

            QrEncoder qrEncoder = new QrEncoder(ErrorCorrectionLevel.M);
            qrEncoder.TryEncode(content, out QrCode qrCode);

            GraphicsRenderer renderer = new GraphicsRenderer(new FixedModuleSize(5, QuietZoneModules.Two), Brushes.Black, Brushes.White);
            using (MemoryStream ms = new MemoryStream())
            {
                renderer.WriteToStream(qrCode.Matrix, ImageFormat.Bmp, ms);
                var bytes = new byte[ms.Length];
                ms.Seek(0, SeekOrigin.Begin);
                ms.Read(bytes, 0, (int)ms.Length);

                return bytes;
            }
        }
    }
}
