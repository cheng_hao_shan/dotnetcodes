﻿using System;
using System.Collections.Generic;

namespace DotNet.Utilities
{
    /// <summary>
    /// 可枚举类型扩展;
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// 按指定字段排除重复项;
        /// https://www.cnblogs.com/niuzaihenmang/p/5620915.html
        /// 示例：peopleList.DistinctBy(i => new { i.UserName }).ToList();
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="Tkey"></typeparam>
        /// <param name="source"></param>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        public static IEnumerable<TSource> DistinctBy<TSource, Tkey>(this IEnumerable<TSource> source, Func<TSource, Tkey> keySelector)
        {
            HashSet<Tkey> hashSet = new HashSet<Tkey>();
            foreach (TSource item in source)
            {
                if (hashSet.Add(keySelector(item)))
                {
                    yield return item;
                }
            }
        }
    }
}
