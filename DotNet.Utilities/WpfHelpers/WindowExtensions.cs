﻿using System.Linq;
using System.Windows;
using System.Windows.Forms;

namespace DotNet.Utilities.WpfHelpers
{
    public static class WindowExtensions
    {
        /// <summary>
        /// 最大化到第二屏;
        /// https://ask.csdn.net/questions/162348
        /// </summary>
        /// <param name="window"></param>
        public static void MaximizeToSecondaryMonitor(this Window window)
        {
            // NB : Best to call this function from the windows Loaded event or after showing the window
            // (otherwise window is just positioned to fill the secondary monitor rather than being maximised).

            var secondaryScreen = Screen.AllScreens.FirstOrDefault(s => !s.Primary);

            if (secondaryScreen != null)
            {
                if (!window.IsLoaded)
                    window.WindowStartupLocation = WindowStartupLocation.Manual;

                var workingArea = secondaryScreen.WorkingArea;
                window.Left = workingArea.Left;
                window.Top = workingArea.Top;
                window.Width = workingArea.Width;
                window.Height = workingArea.Height;
                // If window isn't loaded then maxmizing will result in the window displaying on the primary monitor
                if (window.IsLoaded)
                    window.WindowState = WindowState.Maximized;
            }
        }
    }
}