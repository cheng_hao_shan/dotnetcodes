﻿/*
 * 源码己托管:http://gitee.com/dlgcy/dotnetcodes
 */
using System;
using System.IO;
using System.Threading;
using Newtonsoft.Json;

namespace DLGCY.Utilities
{
    /// <summary>
    /// 虚拟服务器，提供测试数据;
    /// </summary>
    public class FakeServer
    {
        public static string FakeDirectory { get; set; } = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FakeServer");

        /// <summary>
        /// 从JSON文件获取对象;（参考自YT）
        /// </summary>
        /// <typeparam name="TRes">对象类型</typeparam>
        /// <param name="name">文件名（不带后缀.json）</param>
        /// <returns>对象</returns>
        public static TRes GetJsonData<TRes>(string name)
            where TRes:new()
        {           
            string res;
            var file = Path.Combine(FakeDirectory, $"{name}.json");
            if (!File.Exists(file))
            {
                return default(TRes);
            }
            using (var sr = new StreamReader(file))
            {
                res = sr.ReadToEnd();
            }

            Thread.Sleep(500);
            return JsonConvert.DeserializeObject<TRes>(res);
        }
    }
}
