﻿/*
 * 源码己托管:http://gitee.com/dlgcy/dotnetcodes
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace DLGCY.Utilities
{
    /// <summary>
    /// 弹窗;
    /// </summary>
    public class JS_Alert
    {
        /*
         * 使用ScriptManager需引用System.Web.Extensions(可以不写using);
         */

        /// <summary>
        /// 在页面上弹出消息;
        /// </summary>
        /// <param name="page">页面实例</param>
        /// <param name="msg">消息文字</param>
        public static void AlertOnPage(Page page, string msg)
        {
            page.ClientScript.RegisterStartupScript(page.GetType(), "Message", "<script language='javascript' defer='defer'> alert('" + msg.ToString() + "'); </script>");
        }

        /// <summary>
        /// 在页面上弹出消息（版本2:推荐）;
        /// </summary>
        /// <param name="page">页面实例</param>
        /// <param name="msg">消息文字</param>
        public static void AlertOnPage2(Page page, string msg)
        {
            ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "Message", "<script language='javascript' defer='defer'> alert('" + msg.ToString() + "'); </script>", false);
        }

        /// <summary>
        /// 在页面上弹出消息（版本3）;
        /// </summary>      
        public static void AlertOnPage3(Page page, string msg)
        {
            page.ClientScript.RegisterClientScriptBlock(page.GetType(), "Message", "<script language='javascript' defer='defer'> alert('" + msg.ToString() + "'); </script>");
        }

        /// <summary>
        /// 在页面上弹出消息（版本4）;
        /// </summary>
        public static void AlertOnPage4(Page page, string msg)
        {
            ScriptManager.RegisterStartupScript(page, page.GetType(), "Message", "<script language='javascript' defer='defer'> alert('" + msg.ToString() + "'); </script>", false);
        }
    }

    /// <summary>
    /// 跳转;
    /// </summary>
    public class JS_Redirect
    {
        /// <summary>
        /// 使用JS方式进行页面跳转;
        /// </summary>      
        public static void Js_Redirect(Page page, string url)
        {
            //page.Response.Write("<script language=javascript>window.location.href='" + url + "'</script>");
            page.ClientScript.RegisterStartupScript(page.GetType(), "Redirect",
                "<script language=javascript  defer='defer'>window.location.href='" + url + "'</script>");
        }

        /// <summary>
        /// 使用JS方式进行页面跳转（版本2:推荐）;
        /// </summary>      
        public static void Js_Redirect2(Page page, string url)
        {
            ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "Redirect",
                "window.location.href='" + url + "'", true);
        }

        /// <summary>
        /// 使用JS方式进行页面跳转（版本3）;
        /// </summary>      
        public static void Js_Redirect3(Page page, string url)
        {
            page.ClientScript.RegisterClientScriptBlock(page.GetType(), "Redirect",
                "<script language=javascript  defer='defer'>window.location.href='" + url + "'</script>");
        }
    }

    /// <summary>
    /// 刷新;
    /// </summary>
    public class JS_Refresh
    {
        /// <summary>
        /// 刷新当前页面;
        /// </summary>
        /// <param name="page"></param>
        public static void RefreshCurrentPage(Page page)
        {          
            ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "Refresh",
                "window.location.reload()", true);
        }
    }
}
