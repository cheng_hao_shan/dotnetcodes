﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;

namespace DotNet.Utilities
{
    /// <summary>
    /// SQLServer数据库帮助类（来自YB）
    /// </summary>
    public class DBhelper
    {
        public static string ConnStr = ConfigurationManager.ConnectionStrings["connString"].ConnectionString;

        public static DataTable GetTable(string sqlstr, bool recordLog = true)
        {
            if(recordLog) Console.WriteLine($"\r\n[SQL]{sqlstr}");

            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(ConnStr);

            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sqlstr, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        /// <summary>
        /// 增、删、改公共方法
        /// </summary>
        /// <param name="sqlstr"></param>
        /// <returns></returns>
        public static int NonQuery(string sqlstr)
        {
            Console.WriteLine($"\r\n[语句]{sqlstr}", false);

            int val = 0;
            SqlConnection conn = new SqlConnection(ConnStr);
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sqlstr, conn);
                val = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                conn.Close();
            }

            Console.WriteLine($" 受影响行数：{val}");
            return val;
        }

        /// <summary>
        /// 执行查询，并返回由查询返回的结果集中的第一行的第一列
        /// </summary>
        /// <param name="sqlstr"></param>
        /// <param name="memberName"></param>
        /// <returns></returns>
        public static int ExecuteScalar(string sqlstr, [CallerMemberName] string memberName = "")
        {
            Console.WriteLine($"[调用者]{memberName}\r\n[语句]{sqlstr}", false);

            int val = -1;
            SqlConnection conn = new SqlConnection(ConnStr);
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sqlstr, conn);
                val = int.Parse(cmd.ExecuteScalar().ToString());
                Console.WriteLine($" 结果：{val}");
            }
            catch
            {
                throw;
            }
            finally
            {
                conn.Close();
            }

            return val;
        }

        /// <summary>
        /// 执行查询，并返回由查询返回的结果集中的第一行的第一列
        /// </summary>
        /// <param name="sqlstr"></param>
        /// <param name="memberName"></param>
        /// <returns></returns>
        public static string ExecuteScalarStr(string sqlstr, [CallerMemberName] string memberName = "")
        {
            Console.WriteLine($"[调用者]{memberName}\r\n[语句]{sqlstr}", false);

            string result = string.Empty;
            SqlConnection conn = new SqlConnection(ConnStr);

            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sqlstr, conn);
                result = cmd.ExecuteScalar()+"";

                Console.WriteLine($" 结果：{result}");
            }
            catch(Exception ex)
            {
                Console.WriteLine($" 出错：{ex.Message}");
            }
            finally
            {
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// 执行多条SQL语句，实现数据库事务。
        /// </summary>
        /// <param name="SQLStringList">多条SQL语句</param>		
        public static bool SqlTran(ArrayList SQLStringList)
        {
            int index = 0;
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                SqlTransaction tx = conn.BeginTransaction();
                cmd.Transaction = tx;
                try
                {
                    for (int n = 0; n < SQLStringList.Count; n++)
                    {
                        string strsql = SQLStringList[n].ToString();
                        if (strsql.Trim().Length > 1)
                        {
                            cmd.CommandText = strsql;
                            index += cmd.ExecuteNonQuery();
                        }
                    }

                    if (index != SQLStringList.Count)
                    {
                        tx.Rollback();
                        return false;
                    }
                    tx.Commit();
                    return true;
                }
                catch
                {
                    tx.Rollback();
                    return false;
                }
                finally
                {
                    cmd.Dispose();
                    conn.Close();
                }
            }
        }

        /// <summary>
        /// 执行多条SQL语句，实现数据库事务。
        /// </summary>
        /// <param name="SQLStringList">多条SQL语句</param>		
        public static bool SqlTranEx(ArrayList SQLStringList)
        {
            int index = 0;
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                SqlTransaction tx = conn.BeginTransaction();
                cmd.Transaction = tx;
                try
                {
                    for (int n = 0; n < SQLStringList.Count; n++)
                    {
                        string strsql = SQLStringList[n].ToString();
                        if (strsql.Trim().Length > 1)
                        {
                            cmd.CommandText = strsql;
                            index += cmd.ExecuteNonQuery();
                        }
                    }

                    //if (index != SQLStringList.Count)
                    //{
                    //    tx.Rollback();
                    //    return false;
                    //}

                    tx.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    return false;
                }
                finally
                {
                    cmd.Dispose();
                    conn.Close();
                }
            }
        }

        /// <summary>
        ///  执行带参数的查询SQL语句或存储过程
        /// </summary>
        /// <param name="cmdText">查询SQL语句或存储过程</param>
        /// <param name="paras">参数集合</param>
        /// <param name="ct">命令类型</param>
        /// <returns>Table值</returns>
        public static DataTable ExecuteQuery(string cmdText, SqlParameter[] paras, CommandType ct)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(cmdText, conn);
                cmd.CommandType = ct;
                cmd.Parameters.AddRange(paras);

                SqlDataReader sdr = null;
                using (sdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    dt.Load(sdr);
                }
                return dt;
            }
        }
    }
}
