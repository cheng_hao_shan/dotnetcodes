﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;
/*
 * 源码己托管: http://gitee.com/dlgcy/dotnetcodes
 */
namespace DotNet.Utilities
{
    /// <summary>
    /// [dlgcy]SQLServer数据库帮助类
    /// </summary>
    /// <example>
    /// var helper = new SqlServerHelper(); 
    /// helper.Cmd.CommandText = "select * from TableA";    
    /// helper.GetTable();
    /// </example>
    public class SqlServerHelper
    {
        //public string IP { get; set; }
        //public string DbName { get; set; }
        //public string UserName { get; set; }
        //public string Password { get; set; }

        public string ConnStr = ConfigurationManager.ConnectionStrings["connString"].ConnectionString;

        private readonly SqlCommand _Cmd = new SqlCommand();
        public SqlCommand Cmd
        {
            get
            {
                if (_Cmd.Connection == null)
                {
                    //_Cmd.Connection = new SqlConnection($"Data Source={IP};Initial Catalog={DbName};User ID={UserName};Password={Password};Persist Security Info=True;MultipleActiveResultSets=true");
                    _Cmd.Connection = new SqlConnection(ConnStr);
                }

                if (_Cmd.Connection.State != ConnectionState.Open)
                {
                    _Cmd.Connection.Open();
                }

                return _Cmd;
            }
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        public SqlServerHelper()
        {
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="connStr">指定的连接字符串</param>
        public SqlServerHelper(string connStr)
        {
            ConnStr = connStr;
        }

        ~SqlServerHelper()
        {
            Cmd.Connection?.Close();
        }

        /// <summary>
        /// 查询表数据
        /// </summary>
        /// <param name="recordLog">是否记录日志</param>
        /// <param name="memberName">调用者</param>
        /// <returns></returns>
        public DataTable GetTable(bool recordLog = true, [CallerMemberName] string memberName = "")
        {
            if(recordLog) Console.WriteLine($"[调用者]{memberName}\r\n[语句]{Cmd.CommandText}", false);
            DataTable table = new DataTable();

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter { SelectCommand = Cmd };
                adapter.Fill(table);

                if(recordLog) Console.WriteLine($" 查到记录条数：{table.Rows.Count}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                Cmd.Connection.Close();
            }

            return table;
        }

        /// <summary>
        /// 增、删、改公共方法
        /// </summary>
        /// <param name="recordLog">是否记录日志</param>
        /// <param name="memberName">调用者</param>
        /// <returns>出错返回-1</returns>
        public int ExecteNonQuery(bool recordLog = true, [CallerMemberName] string memberName = "")
        {
            if(recordLog) Console.WriteLine($"[调用者]{memberName}\r\n[语句]{Cmd.CommandText}", false);

            int val = -1;
            try
            {
                val = Cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                Cmd.Connection.Close();
            }

            if(recordLog) Console.WriteLine($" 受影响行数：{val}");
            return val;
        }

        /// <summary>
        /// 执行查询，并返回结果集中的第一行第一列的字符串形式
        /// </summary>
        /// <param name="recordLog">是否记录日志</param>
        /// <param name="memberName">调用者</param>
        /// <returns></returns>
        public string ExecuteScalarStr(bool recordLog = true, [CallerMemberName] string memberName = "")
        {
            if(recordLog) Console.WriteLine($"[调用者]{memberName}\r\n[语句]{Cmd.CommandText}", false);

            string result = string.Empty;

            try
            {
                result = Cmd.ExecuteScalar() + "";

                if(recordLog) Console.WriteLine($" 结果：{result}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                Cmd.Connection.Close();
            }

            return result;
        }

        /// <summary>
        /// 获取列最大值加1;
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="columnName">列名</param>
        /// <param name="recordLog">是否记录日志</param>
        /// <param name="memberName">调用者</param>
        /// <returns></returns>
        public int GetMaxIdPlus(string tableName, string columnName, bool recordLog = true, [CallerMemberName] string memberName = "")
        {
            int result = 0;

            Cmd.CommandText = $@"select max({columnName}) from {tableName}";
            if (recordLog) Console.WriteLine($"[调用者]{memberName}\r\n[语句]{Cmd.CommandText}", false);

            try
            {
                string scalar = Cmd.ExecuteScalar() + "";
                if (!string.IsNullOrEmpty(scalar))
                {
                    int.TryParse(scalar, out result);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                Cmd.Connection.Close();
            }

            result++;
            if (recordLog) Console.WriteLine($" 结果：{result}");

            return result;
        }

        void Example()
        {
            var helper = new SqlServerHelper();
            helper.Cmd.CommandText = "select * from TableA";
            DataTable dt = helper.GetTable();
            var list = dt.ConvertToModel<object>();
        }
	}
}
