﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace DotNet.Utilities
{
    /// <summary>   
    /// 实体转换辅助类
    /// https://www.cnblogs.com/shiyh/p/7478241.html
    /// </summary>   
    public static class ModelConvertHelper
    {
        public static IList<T> ConvertToModel<T>(this DataTable dt)
            where T : new()
        {
            // 定义集合   
            IList<T> ts = new List<T>();

            foreach (DataRow dr in dt.Rows)
            {
                T t = new T();

                // 获得此模型的公共属性     
                PropertyInfo[] propertys = t.GetType().GetProperties();
                foreach (PropertyInfo pi in propertys)
                {
                    string tempName = pi.Name;

                    if (dt.Columns.Contains(tempName))
                    {
                        // 判断此属性是否有Setter     
                        if (!pi.CanWrite) continue;

                        object value = dr[tempName];
                        if (value != DBNull.Value)
                            pi.SetValue(t, value, null);
                    }
                }

                ts.Add(t);
            }

            return ts;
        }
    }
}