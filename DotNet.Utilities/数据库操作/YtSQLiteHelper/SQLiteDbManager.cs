﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
/*
 * 代码已托管：https://gitee.com/dlgcy/dotnetcodes/tree/dlgcy/DotNet.Utilities/%E6%95%B0%E6%8D%AE%E5%BA%93%E6%93%8D%E4%BD%9C/YtSQLiteHelper
 */
namespace DotNet.Utilities.YtSQLiteHelper
{
    public static class SQLiteDbManager
    {
        /// <summary>
        /// 数据库所在文件夹（使用相对路径则此处留空）
        /// </summary>
        public static string DbDir = "";
        private const string DbName = "Data.db";
        private static Dictionary<string, SQLiteConnection> dbMapping;
        private static readonly List<Type> HasAuthTypes = new List<Type>();

        /// <summary>
        /// 设置数据库文件夹;
        /// </summary>
        /// <param name="dbDir"></param>
        public static void SetDbDir(string dbDir)
        {
            DbDir = dbDir;
        }

        /// <summary>
        /// 获取连接
        /// </summary>
        /// <param name="dbName">数据库名（默认为"Data"）</param>
        /// <returns></returns>
        private static SQLiteConnection GetConnection(string dbName = DbName)
        {
            if (!dbName.EndsWith(".db"))
            {
                dbName = $"{dbName}.db";
            }

            string totalPath;
            if (string.IsNullOrEmpty(DbDir))
            {
                totalPath = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, dbName));
            }
            else
            {
                totalPath = Path.GetFullPath(Path.Combine(DbDir, dbName));
            }

            if (dbMapping == null)
            {
                dbMapping = new Dictionary<string, SQLiteConnection>();
            }

            if (!dbMapping.ContainsKey(dbName))
            {
                var dir = Path.GetDirectoryName(totalPath);
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }

                dbMapping[dbName] = new SQLiteConnection(totalPath);
            }

            return dbMapping[dbName];
        }

        private static void SureTable<T>(string dbName)
        {
            var arr = new[] { typeof(int) };
            if (arr.Contains(typeof(T)))
            {
                return;
            }

            var tp = typeof(T);

            if (!HasAuthTypes.Contains(tp))
            {
                var conn = GetConnection(dbName);
                conn.CreateTable<T>();
                HasAuthTypes.Add(tp);
            }
        }

        public static void Insert<T>(string dbName, T data)
        {
            Console.WriteLine($"\r\n[SQL][{dbName}].[{typeof(T)}]将插入：\r\n{JsonConvert.SerializeObject(data)}");
            var conn = GetConnection(dbName);
            SureTable<T>(dbName);
            conn.Insert(data);
        }

        public static void Insert<T>(T data)
        {
            Insert(DbName, data);
        }

        public static void InsertAll<T>(string dbName, IEnumerable<T> collection)
        {
            Console.WriteLine($"\r\n[SQL][{dbName}].[{typeof(T)}]将插入全部：\r\n{JsonConvert.SerializeObject(collection)}");
            var conn = GetConnection(dbName);
            SureTable<T>(dbName);
            conn.InsertAll(collection);
        }

        public static List<T> Query<T>(string query, params object[] args) where T : new()
        {
            return Query<T>(DbName, query, args);
        }

        public static List<T> Query<T>(string dbName, string query, params object[] args) where T : new()
        {
            Console.WriteLine($"\r\n[SQL][{dbName}].[{typeof(T)}]将查询：\r\n{query}\r\n参数：{JsonConvert.SerializeObject(args)}");
            var conn = GetConnection(dbName);
            SureTable<T>(dbName);
            return conn.Query<T>(query, args);
        }

        /// <summary>
        /// 执行语句（字段判断不支持中文）
        /// </summary>
        /// <param name="query"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static int Excute(string query, params object[] args)
        {
            return Excute(DbName, query, args);
        }

        public static int Excute(string dbName, string query, params object[] args)
        {
            Console.WriteLine($"\r\n[SQL][{dbName}]将执行：\r\n{query}\r\n参数：{JsonConvert.SerializeObject(args)}");
            var conn = GetConnection(dbName);
            int num = conn.Execute(query, args);
            Console.WriteLine($"[SQL]受影响行数：{num}");
            return num;
        }
    }
}
