﻿using System;

namespace DotNet.Utilities.YtSQLiteHelper
{
    public abstract class BaseTable
    {
        [PrimaryKey, AutoIncrement, ColumnOrder(0)]
        public int Id { get; set; }

        [Indexed, ColumnOrder(1)]
        public DateTime OperateDateTime { get; set; } = DateTime.Now;
    }

    public class TestTable : BaseTable
    {
        [ColumnOrder(2)]
        public string Name { get; set; }
    }
}