﻿using System;
using System.IO;
using System.Net;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace DotNet.Utilities
{
    public static class WebServiceXmlHelper
    {
        /// <summary>
        /// 处理方法;
        /// </summary>
        /// <typeparam name="TReq">请求类型</typeparam>
        /// <typeparam name="TRes">响应类型</typeparam>
        /// <param name="req">请求类型参数</param>
        /// <param name="tradeFunc">执行方法（按实际情况修改，可传递WebService的具体方法）</param>
        /// <returns>响应类型数据</returns>
        public static TRes Handler<TReq, TRes>(TReq req, Func<string, string> tradeFunc)
            where TReq : ReqBase, new()
            where TRes : ResBase, new()
        {
            try
            {
                string sendStr = Serializer(req);
                sendStr = sendStr.Replace("\r", "").Replace("\n", "").Trim();
                LogHelper.Write($"【发送】{sendStr}");

                string recvStr = tradeFunc(sendStr);
                LogHelper.Write($"【接收】{recvStr}", false);

                var data = DeSerializer<TRes>(recvStr);
                return data;
            }

            catch (Exception ex)
            {
                LogHelper.Write($"{ex}");
                return new TRes();
            }
        }

        #region Post方法

        /// <summary>
        /// 处理方法(异步);
        /// </summary>
        /// <typeparam name="TReq">请求类型</typeparam>
        /// <typeparam name="TRes">响应类型</typeparam>
        /// <param name="req">请求类型参数</param>
        /// <returns>Task</returns>
        public static async Task<TRes> HandlerAsync<TReq, TRes>(TReq req)
            where TReq : ReqBase, new()
            where TRes : ResBase, new()
        {
            try
            {
                string url = ""; //服务地址;
                string sendStr = Serializer(req);
                LogHelper.Write($"\r\n【发送】{GetShortStr(sendStr)}");

                WebClient wc = new WebClient();
                ServicePointManager.Expect100Continue = false;
                wc.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                var ret = await wc.UploadDataTaskAsync(url, Encoding.UTF8.GetBytes(sendStr));
                string recvStr = Encoding.UTF8.GetString(ret);
                LogHelper.Write($"【接收】{GetShortStr(recvStr)}");

                var data = DeSerializer<TRes>(recvStr);
                return data;
            }
            catch (Exception ex)
            {
                LogHelper.Write($"{ex}");
                return new TRes();
            }
        }

        private static long GetTimeStamp()
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds);
        }

        /// <summary>
        /// 缩减字符串
        /// </summary>
        /// <param name="content"></param>
        /// <param name="maxLen"></param>
        /// <returns></returns>
        private static string GetShortStr(string content, int maxLen = 5000)
        {
            try
            {
                int len = content.Length;
                if (len > maxLen)
                {
                    content = $"{content.Substring(0, maxLen / 2)}【...】{content.Substring(len - maxLen / 2)}";
                }
            }
            catch
            {
                // ignored
            }

            return content;
        }

        #endregion

        /// <summary>
        /// 反序列化;
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="str"></param>
        /// <returns></returns>
        public static T DeSerializer<T>(string str) where T : class
        {
            try
            {
                using (StringReader sr = new StringReader(str))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(T));
                    return serializer.Deserialize(sr) as T;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Write($"{ex}");
                return null;
            }
        }

        /// <summary>
        /// 序列化;
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string Serializer<T>(T obj)
        {
            try
            {
                //强制指定命名空间，覆盖默认的命名空间  
                XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
                namespaces.Add(string.Empty, string.Empty);

                using (MemoryStream ms = new MemoryStream())
                using (XmlTextWriter writer = new XmlTextWriter(ms, Encoding.UTF8))
                {
                    writer.Formatting = Formatting.Indented;

                    XmlSerializer xs = new XmlSerializer(typeof(T));
                    xs.Serialize(writer, obj, namespaces);

                    byte[] temp = ms.ToArray();
                    string xml = new UTF8Encoding().GetString(temp).Trim();
                    return xml;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Write($"{ex}");
                return string.Empty;
            }
        }
    }

    /// <summary>
    /// 请求参数基类;
    /// </summary>
    [XmlRoot("interface")]
    public class ReqBase
    {

    }

    /// <summary>
    /// 返回参数基类;
    /// </summary>
    [XmlRoot("response")]
    public class ResBase
    {
        [XmlIgnore]
        public bool Success => result == "0";

        /// <summary>
        /// 返回结果 (0,成功 否则,失败)
        /// </summary>
        public string result { get; set; }

        /// <summary>
        /// 返回结果描述
        /// </summary>
        public string resultdesc { get; set; }
    }
}
