﻿using System;
using System.Text.RegularExpressions;
/*
 * https://gitee.com/dlgcy/dotnetcodes
 */
namespace DotNet.Utilities
{
    /// <summary>
    /// 身份证号帮助类;
    /// </summary>
    public static class IdCardNoHelper
    {
        #region https://www.cnblogs.com/fjptwwf/p/5687255.html

        /// <summary>
        /// 根据身份证获取生日
        /// </summary>
        /// <param name="cardId">身份证</param>
        /// <param name="isFormat">是否有格式(true:1990-01-01,false:19900101),默认为true</param>
        /// <returns></returns>
        public static string GetBirthdayByIdCardNo(string cardId, bool isFormat = true)
        {
            string birthday = string.Empty;

            if (string.IsNullOrEmpty(cardId))
            {
                return birthday;
            }

            try
            {
                System.Text.RegularExpressions.Regex regex = null;
                if (cardId.Length == 18)
                {
                    regex = new Regex(@"^\d{17}(\d|x|X)$");
                    if (regex.IsMatch(cardId))
                    {
                        birthday = isFormat ? cardId.Substring(6, 8).Insert(4, "-").Insert(7, "-") : cardId.Substring(6, 8);
                    }
                    else
                    {
                        birthday = "Invalid CardId";
                    }
                }
                else if (cardId.Length == 15)
                {
                    regex = new Regex(@"^\d{15}");
                    if (regex.IsMatch(cardId))
                    {
                        if (isFormat)
                            birthday = cardId.Substring(6, 6).Insert(2, "-").Insert(5, "-");
                        else
                            birthday = cardId.Substring(6, 6);
                    }
                    else
                    {
                        birthday = "Invalid CardId";
                    }
                }
                else
                {
                    birthday = "Invalid CardId";
                }
            }
            catch (Exception ex)
            {
                birthday = ex.Message;
            }

            return birthday;
        }

        /// <summary>
        /// 根据身份证获取身份证信息
        /// 
        /// 18位身份证:
        /// 0-地区代码(1~6位,其中1、2位数为各省级政府的代码，3、4位数为地、市级政府的代码，5、6位数为县、区级政府代码)
        /// 1-出生年月日(7~14位)
        /// 2-顺序号(15~17位单数为男性分配码，双数为女性分配码)
        /// 3-性别(男/女)
        /// 4-性别("1"-男,"2"-女)
        /// 
        /// 15位身份证:
        /// 0-地区代码 
        /// 1-出生年份(7~8位年,9~10位为出生月份，11~12位为出生日期 
        /// 2-顺序号(13~15位)，并能够判断性别，奇数为男，偶数为女
        /// 3-性别(男/女)
        /// 4-性别("1"-男,"2"-女)
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public static string[] GetIdCardInfo(string cardId)
        {
            string[] info = new string[5];

            if (string.IsNullOrEmpty(cardId))
            {
                return info;
            }

            try
            {
                System.Text.RegularExpressions.Regex regex = null;

                if (cardId.Length == 18)
                {
                    regex = new Regex(@"^\d{17}(\d|x|X)$");
                    if (regex.IsMatch(cardId))
                    {
                        info[0] = cardId.Substring(0, 6);
                        info[1] = cardId.Substring(6, 8);
                        info[2] = cardId.Substring(14, 3);
                    }
                }
                else if (cardId.Length == 15)
                {
                    regex = new Regex(@"^\d{15}");
                    if (regex.IsMatch(cardId))
                    {
                        info[0] = cardId.Substring(0, 6);
                        info[1] = cardId.Substring(6, 6);
                        info[2] = cardId.Substring(12, 3);
                    }
                }

                //性别;
                info[3] = Convert.ToInt32(info[2]) % 2 != 0 ? "男" : "女";
                info[4] = info[3] == "男" ? "1" : "2";
            }
            catch (Exception ex)
            {
                info[0] = ex.Message;
            }

            return info;
        }

        #endregion

        /// <summary>
        /// 根据身份证获取身份证信息（精简版-只判断18位身份证）
        /// 【0-地区代码(1~6位,其中1、2位数为各省级政府的代码，3、4位数为地、市级政府的代码，5、6位数为县、区级政府代码)】
        /// 【1-出生年月日(7~14位)】
        /// 【2-顺序号(15~17位单数为男性分配码，双数为女性分配码)】
        /// 【3-性别(男/女)】
        /// 【4-性别("1"-男,"2"-女)】
        /// 【5-出生年月日("2000-01-01")】
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public static string[] GetIdCardInfoSimple(string cardId)
        {
            string[] info = new string[6];

            if (string.IsNullOrEmpty(cardId))
            {
                return info;
            }

            try
            {
                if (cardId.Length == 18)
                {
                    var regex = new Regex(@"^\d{17}(\d|x|X)$");
                    if (regex.IsMatch(cardId))
                    {
                        info[0] = cardId.Substring(0, 6);
                        info[1] = cardId.Substring(6, 8);
                        info[2] = cardId.Substring(14, 3);

                        info[3] = Convert.ToInt32(info[2]) % 2 != 0 ? "男" : "女";
                        info[4] = info[3] == "男" ? "1" : "2";
                        info[5] = info[1].Insert(4, "-").Insert(7, "-");
                    }
                    else
                    {
                        info[0] = "身份证号末位不符";
                    }
                }
                else
                {
                    info[0] = "身份证号长度不符";
                }

            }
            catch (Exception ex)
            {
                info[0] = ex.Message;
            }

            return info;
        }

        #region https://www.jb51.net/article/119502.htm

        /// <summary>
        /// 验证18位身份证号;
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool CheckIdCard18(string id)
        {
            try
            {
                if (long.TryParse(id.Remove(17), out var n) == false || n < Math.Pow(10, 16) || long.TryParse(id.Replace('x', '0').Replace('X', '0'), out n) == false)
                {
                    return false;
                }

                string address = "11x22x35x44x53x12x23x36x45x54x13x31x37x46x61x14x32x41x50x62x15x33x42x51x63x21x34x43x52x64x65x71x81x82x91";
                if (address.IndexOf(id.Remove(2), StringComparison.Ordinal) == -1)
                {
                    return false;
                }

                string birth = id.Substring(6, 8).Insert(6, "-").Insert(4, "-");
                DateTime time = new DateTime();
                if (DateTime.TryParse(birth, out time) == false)
                {
                    return false;
                }

                string[] arrVarifyCode = ("1,0,x,9,8,7,6,5,4,3,2").Split(',');
                string[] wi = ("7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2").Split(',');
                char[] ai = id.Remove(17).ToCharArray();
                int sum = 0;
                for (int i = 0; i < 17; i++)
                {
                    sum += int.Parse(wi[i]) * int.Parse(ai[i].ToString());
                }
                Math.DivRem(sum, 11, out var y);
                if (arrVarifyCode[y] != id.Substring(17, 1).ToLower())
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 验证15位身份证号;
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool CheckIdCard15(string id)
        {
            try
            {
                if (long.TryParse(id, out var n) == false || n < Math.Pow(10, 14))
                {
                    return false;
                }

                string address = "11x22x35x44x53x12x23x36x45x54x13x31x37x46x61x14x32x41x50x62x15x33x42x51x63x21x34x43x52x64x65x71x81x82x91";
                if (address.IndexOf(id.Remove(2), StringComparison.Ordinal) == -1)
                {
                    return false;
                }

                string birth = id.Substring(6, 6).Insert(4, "-").Insert(2, "-");
                DateTime time = new DateTime();
                if (DateTime.TryParse(birth, out time) == false)
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        #endregion
    }
}
