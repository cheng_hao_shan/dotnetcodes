﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Text;
using System.Management;

namespace DotNet.Utilities.Printer
{
    public class PrintHelper
    {
        private readonly string printerName;
        private readonly Queue<IPrintable> printQueue;

        public PrintHelper(Queue<IPrintable> queue, string printer = "Microsoft Print to PDF")
        {
            printerName = printer;
            printQueue = queue;
        }

        public bool Print()
        {
            try
            {
                var docToPrint = new PrintDocument();
                docToPrint.PrintPage += docToPrint_PrintPage;
                docToPrint.DefaultPageSettings.PaperSize = GetPageSize(docToPrint.DefaultPageSettings.PaperSize, 70);

                docToPrint.PrinterSettings.PrinterName = printerName;
                docToPrint.PrintController = new StandardPrintController();
                var content = new StringBuilder();
                foreach (var printable in printQueue)
                {
                    content.AppendLine(GetPrintableContent(printable));
                }

                //Logger.Printer.Info($"[{printerName}]开始打印:\r\n{content}");
                docToPrint.Print();
                //Logger.Printer.Info($"[{printerName}]打印完成");
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private string GetPrintableContent(IPrintable printable)
        {
            return printable.GetLogText();
            //var type = printable.GetType();
            //if (type == typeof(PrintItemImage))
            //{
            //    return "[图片]";
            //}
            //if (type == typeof(PrintItemTriText))
            //{
            //    var obj = printable as PrintItemTriText;
            //    return $"{obj.Text} {obj.Text2} {obj.Text3}";
            //}
            //if (type == typeof(PrintItemText))
            //{
            //    var obj = printable as PrintItemText;
            //    return obj.Text;
            //}
            //if (type == typeof(PrintItemGap))
            //{
            //    return "\n";
            //}
            //return "[Error]" + printable.GetType();
        }

        public PaperSize GetPageSize(PaperSize p, double w = 46.5, double h = 10000)
        {
            //毫米与英寸的转换  1mm= 0.039英寸
            var width = (int)(w * 0.04 * 100);
            var height = (int)(h * 0.04 * 100);
            //设置时高和宽的单位是百分之一英寸,比如设置为宽度5英寸,高度10英寸
            if (p.Height > height)
            {
                height = p.Height;
            }
            var ps = new PaperSize("Custom", width, height);
            return ps;
        }

        private void docToPrint_PrintPage(object sender, PrintPageEventArgs e)
        {
            //Logger.Printer.Info(printerName + " 页面 " + printQueue.Count);
            var top = 0f;
            int count = 0;
            while (printQueue.Count > 0)
            {
                if ((top + printQueue.Peek().GetHeight(e.Graphics, e.PageBounds.Width) > e.PageBounds.Height) && count > 0)
                    break;
                top += printQueue.Dequeue().Print(e.Graphics, top, e.PageBounds.Width);
                count++;
            }
            e.HasMorePages = printQueue.Count > 0;

            //Logger.Printer.Info(printerName + " 页面打印结束 " + printQueue.Count);
        }

        public static Queue<IPrintable> NewQueue(string title, string title2)
        {
            var queue = new Queue<IPrintable>();
            queue.Enqueue(new PrintItemText
            {
                Text = title,
                StringFormat = PrintConfig.Center,
                Font = PrintConfig.HeaderFont
            });

            if (!string.IsNullOrWhiteSpace(title2))
            {
                queue.Enqueue(new PrintItemGap { Gap = 5f });
                queue.Enqueue(new PrintItemText
                {
                    Text = title2,
                    StringFormat = PrintConfig.Center,
                    Font = PrintConfig.HeaderFont2
                });
            }

            queue.Enqueue(new PrintItemGap { Gap = 10f });
            return queue;
        }

        public static string GetDefaultPrinter()
        {
            return new PrintDocument().PrinterSettings.PrinterName;
        }

        public static List<string> GetAllPrinterName()
        {
            List<string> nameList = new List<string>();

            #region Win7不支持？

            //string strPrinter = "win32_printer";

            //// 获取本地所有打印机
            //ManagementClass mc = new ManagementClass(strPrinter);

            //// 获取所有打印机实例的集合
            //ManagementObjectCollection moc = mc.GetInstances();

            //// 遍历本地所有打印机
            //foreach (ManagementObject printer in moc)
            //{
            //    // 打印机名字
            //    string strName = printer.Properties["DeviceId"].Value.ToString();
            //    // 打印机状态
            //    //string strState = printer.Properties["PrinterStatus"].Value.ToString();

            //    nameList.Add(strName);
            //}

            #endregion

            foreach (string printName in PrinterSettings.InstalledPrinters)
            {
                if (!nameList.Contains(printName))
                {
                    nameList.Add(printName);
                }
            }

            return nameList;
        }
    }
}