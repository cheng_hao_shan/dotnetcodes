﻿using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace DotNet.Utilities.配置文件操作类
{
    /// <summary>
    /// .ini 配置文件操作类;
    /// </summary>
    public class IniHelper
    {
        /// <summary>
        /// INI文件的绝对路径
        /// </summary>
        public string Path;

        /// <summary>
        /// 读取的默认值
        /// </summary>
        public string DefValue;

        /// <summary>设定INI文件中的属性</summary>
        /// <param name="section">节</param>
        /// <param name="key">键</param>
        /// <param name="val">值</param>
        /// <param name="filePath">INI文件的绝对地址</param>
        /// <returns></returns>
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        /// <summary>读取INI文件中的属性</summary>
        /// <param name="section">节</param>
        /// <param name="key">键</param>
        /// <param name="def">默认值</param>
        /// <param name="retVal">被存储到的StringBuilder</param>
        /// <param name="size">最大字串截取长度</param>
        /// <param name="filePath">INI文件的绝对地址</param>
        /// <returns></returns>
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        /// <summary>INI读写工具类</summary>
        /// <param name="path">INI文件绝对地址</param>
        /// <param name="def">读取值的默认值</param>
        public IniHelper(string path, string def = "")
        {
            this.Path = new FileInfo(path).FullName;
            this.DefValue = def;
        }

        /// <summary>设置INI文件的一个值</summary>
        /// <param name="section">节</param>
        /// <param name="key">键</param>
        /// <param name="value">值</param>
        public void SetValue(string section, string key, object value)
        {
            if (File.Exists(this.Path))
                IniHelper.WritePrivateProfileString(section, key, value.ToString(), this.Path);
        }

        /// <summary>获取INI文件的一个值</summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public string GetValue(string section, string key, string defaultValue = "")
        {
            if (!File.Exists(this.Path))
                return defaultValue;
            StringBuilder retVal = new StringBuilder((int)byte.MaxValue);
            IniHelper.GetPrivateProfileString(section, key, this.DefValue, retVal, (int)byte.MaxValue, this.Path);
            return retVal.ToString();
        }
    }
}
