﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Utilities.dlgcy
{
    public class NetFileHelper
    {
        /// <summary>
        /// 下载网络文件
        /// （改编自：https://download.csdn.net/download/xiurui12345/4979798）
        /// </summary>
        /// <param name="netFileUrl">网络文件url</param>
        /// <param name="fileName">生成文件名，默认为"yyyyMMddHHmmssffff"</param>
        /// <returns>返回下载完成的文件的本地路径，出错则为null</returns>
        public static string DownloadNetFile(string netFileUrl, string fileName = null)
        {
            string fileFullPath = GetDirectoryFullPath();
            string localFilePath = $"{fileFullPath}" +
                $"{fileName ?? DateTime.Now.ToString("yyyyMMddHHmmssffff")}{netFileUrl.Substring(netFileUrl.LastIndexOf(".", StringComparison.Ordinal))}";
            try
            {
                WebRequest webReq = WebRequest.Create(netFileUrl);
                WebResponse webRes = webReq.GetResponse();
                var fileLength = webRes.ContentLength;

                Stream srm = webRes.GetResponseStream();
                StreamReader srmReader = new StreamReader(srm);
                byte[] bufferBytes = new byte[fileLength];
                int allByte = bufferBytes.Length;
                int startByte = 0;
                while (fileLength > 0)
                {
                    int downByte = srm.Read(bufferBytes, startByte, allByte);
                    if (downByte == 0) { break; };
                    startByte += downByte;
                    allByte -= downByte;
                    //float part = (float)startByte / 1024;
                    //float total = (float)bufferbyte.Length / 1024;
                    //int percent = Convert.ToInt32((part / total) * 100);
                }

                FileStream fs = new FileStream(localFilePath, FileMode.OpenOrCreate, FileAccess.Write);
                fs.Write(bufferBytes, 0, bufferBytes.Length);
                srm.Close();
                srmReader.Close();
                fs.Close();
                return localFilePath;
            }
            catch
            {
                return null;
            }
        }

        private static string GetDirectoryFullPath()
        {
            string currentDomain = AppDomain.CurrentDomain.BaseDirectory;
            string filePath = $"Download\\{DateTime.Now.Year}\\{DateTime.Now.Month}\\";
            string fileFullPath = Path.Combine(currentDomain, filePath);

            if (!Directory.Exists(fileFullPath))
            {
                Directory.CreateDirectory(fileFullPath);
            }

            return fileFullPath;
        }
    }
}
