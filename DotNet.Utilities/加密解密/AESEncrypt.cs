﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Utilities
{
    public class AESEncrypt
    {
        //for test
        //public static readonly string AESKEY = "!eBI$MIs(201488)";
        public static readonly string AESKEY = "!eBI$HXN(201488)";

        public static string Encrypt(string originalPayload)
        {
            string strKey = AESKEY;
            using (var aes = new AesCryptoServiceProvider()
            {
                Key = Encoding.UTF8.GetBytes(strKey),
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            })
            {
                var input = Encoding.UTF8.GetBytes(originalPayload);
                aes.GenerateIV();
                var iv = aes.IV;
                using (var encrypter = aes.CreateEncryptor(aes.Key, iv))
                using (var cipherStream = new MemoryStream())
                {
                    using (var tCryptoStream = new CryptoStream(cipherStream, encrypter, CryptoStreamMode.Write))
                    using (var tBinaryWriter = new BinaryWriter(tCryptoStream))
                    {
                        //Prepend IV to data
                        tBinaryWriter.Write(iv);
                        tBinaryWriter.Write(input);
                        tCryptoStream.FlushFinalBlock();
                    }

                    return Convert.ToBase64String(cipherStream.ToArray());
                }

            }
        }

        public static string Encrypt(string originalPayload, string strKey)
        {
            using (var aes = new AesCryptoServiceProvider()
            {
                Key = Encoding.UTF8.GetBytes(strKey),
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            })
            {
                var input = Encoding.UTF8.GetBytes(originalPayload);
                aes.GenerateIV();
                var iv = aes.IV;
                using (var encrypter = aes.CreateEncryptor(aes.Key, iv))
                using (var cipherStream = new MemoryStream())
                {
                    using (var tCryptoStream = new CryptoStream(cipherStream, encrypter, CryptoStreamMode.Write))
                    using (var tBinaryWriter = new BinaryWriter(tCryptoStream))
                    {
                        //Prepend IV to data
                        tBinaryWriter.Write(iv);
                        tBinaryWriter.Write(input);
                        tCryptoStream.FlushFinalBlock();
                    }

                    return Convert.ToBase64String(cipherStream.ToArray());
                }
            }
        }

        public static string Decrypt(string input)
        {
            string strKey = AESKEY;
            var inputData = Convert.FromBase64String(input);
            var aes = new AesCryptoServiceProvider()
            {
                Key = Encoding.UTF8.GetBytes(strKey),
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            };

            //get first 16 bytes of IV and use it to decrypt
            var iv = new byte[16];
            Array.Copy(inputData, 0, iv, 0, iv.Length);
            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, aes.CreateDecryptor(aes.Key, iv), CryptoStreamMode.Write))
                using (var binaryWriter = new BinaryWriter(cs))
                {
                    //Decrypt Cipher Text from Message
                    binaryWriter.Write(
                        inputData,
                        iv.Length,
                        inputData.Length - iv.Length
                    );
                }

                return Encoding.UTF8.GetString(ms.ToArray());
            }
        }

        public static string Decrypt(string input, string strKey)
        {
            var inputData = Convert.FromBase64String(input);
            var aes = new AesCryptoServiceProvider()
            {
                Key = Encoding.UTF8.GetBytes(strKey),
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            };

            //get first 16 bytes of IV and use it to decrypt
            var iv = new byte[16];
            Array.Copy(inputData, 0, iv, 0, iv.Length);
            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, aes.CreateDecryptor(aes.Key, iv), CryptoStreamMode.Write))
                using (var binaryWriter = new BinaryWriter(cs))
                {
                    //Decrypt Cipher Text from Message
                    binaryWriter.Write(
                        inputData,
                        iv.Length,
                        inputData.Length - iv.Length
                    );
                }

                return Encoding.UTF8.GetString(ms.ToArray());
            }
        }

        public static void EncryptTest()
        {
            string text = "page=PieChart.aspx?RId=datagrid&UId=nfei";
            //string text = "id=datagrid";
            string key = "thisisatestkey__";
            var data = AESEncrypt.Encrypt(text, key);
            Console.WriteLine($"明文：{text}");
            Console.WriteLine($"密文：{data}");

            var decrryptData = AESEncrypt.Decrypt(data, key);

            Console.WriteLine($"解密结果：{decrryptData}");

            Console.ReadLine();
        }

    }
}
