﻿/*
 * 源码己托管:http://gitee.com/dlgcy/dotnetcodes
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Utilities
{
    /// <summary>
    /// 路径/目录/文件夹 帮助类;
    /// </summary>
    public static class PathHelper
    {
        /// <summary>
        /// 通过路径获得绝对路径;
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetAbsPathFromPath(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                if (path.StartsWith("http") || path.StartsWith("ftp:") || path.StartsWith("file:") || Path.IsPathRooted(path))
                {
                }
                else
                {
                    path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path);
                }
            }

            return path;
        }

        /// <summary>
        /// 通过路径获得绝对URI;
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static Uri GetUriFromPath(string path)
        {
            Uri uri = null;
            path = GetAbsPathFromPath(path);
 
            if (!string.IsNullOrWhiteSpace(path))
            {
                uri = new Uri(path, UriKind.Absolute);
            }
 
            return uri;
        }

        #region 删除文件

        /// <summary>
        /// [dlgcy]删除指定文件夹中指定天数之前的文件;
        /// </summary>
        /// <param name="dir">文件夹</param>
        /// <param name="days">大于等于0的天数</param>
        /// <returns>删除的文件数</returns>
        public static int DeleteOldFiles(string dir, int days)
        {
            int count = 0;

            try
            {
                if (!Directory.Exists(dir) || days < 0) return count;

                DirectoryInfo dirInfo = new DirectoryInfo(dir);
                foreach (FileInfo fileInfo in dirInfo.GetFiles())
                {
                    if (fileInfo.CreationTime.AddDays(days).Date <= DateTime.Today)
                    {
                        fileInfo.Delete();
                        count++;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex}");
            }

            return count;
        }

        /// <summary>
        /// [dlgcy]删除指定文件夹的子目录中指定天数之前的文件;
        /// </summary>
        /// <param name="dir">文件夹</param>
        /// <param name="days">大于等于0的天数</param>
        /// <param name="searchOption">搜索子目录的方式，仅当前目录还是包括所有子目录，默认为仅在当前目录搜索</param>
        /// <returns>删除的文件所在目录及对应数量的字典</returns>
        public static Dictionary<string, int> DeleteOldFilesInSubDir(string dir, int days, SearchOption searchOption = SearchOption.TopDirectoryOnly)
        {
            Dictionary<string, int> delDirAndCountDic = new Dictionary<string, int>();

            try
            {
                if (!Directory.Exists(dir) || days < 0) return delDirAndCountDic;

                DirectoryInfo dirInfo = new DirectoryInfo(dir);
                foreach (var directoryInfo in dirInfo.GetDirectories("*", searchOption))
                {
                    string path = directoryInfo.FullName;
                    int count = DeleteOldFiles(path, days);
                    if (count > 0)
                    {
                        delDirAndCountDic.Add(path, count);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex}");
            }

            return delDirAndCountDic;
        }

        /// <summary>
        /// [dlgcy]删除指定文件夹及其子目录中指定天数之前的文件。
        /// 附-记录信息：
        /// if (delDic.Count > 0)
        /// {
        ///     StringBuilder sb = new StringBuilder();
        ///     foreach (var pair in delDic)
        ///     {
        ///         sb.AppendLine($"删除了目录[{pair.Key}]下的[{pair.Value}]个文件 ");
        ///     }
        /// 
        ///     Console.WriteLine(sb.ToString());
        /// }
        /// </summary>
        /// <param name="dir">文件夹</param>
        /// <param name="days">大于等于0的天数</param>
        /// <param name="searchOption">搜索子目录的方式，仅当前目录还是包括所有子目录，默认为仅在当前目录搜索</param>
        /// <returns>删除的文件所在目录及对应数量的字典</returns>
        public static Dictionary<string, int> DeleteOldFilesInDirAndSubDir(string dir, int days, SearchOption searchOption = SearchOption.TopDirectoryOnly)
        {
            Dictionary<string, int> delDirAndCountDic = new Dictionary<string, int>();

            try
            {
                if (!Directory.Exists(dir) || days < 0) return delDirAndCountDic;

                int count = DeleteOldFiles(dir, days);
                if (count > 0)
                {
                    delDirAndCountDic.Add(dir, count);
                }

                var delDic = DeleteOldFilesInSubDir(dir, days, searchOption);
                if (delDic.Count > 0)
                {
                    delDirAndCountDic = delDirAndCountDic.Concat(delDic).ToDictionary(x => x.Key, x => x.Value);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex}");
            }

            return delDirAndCountDic;
        }

        #endregion
    }
}
