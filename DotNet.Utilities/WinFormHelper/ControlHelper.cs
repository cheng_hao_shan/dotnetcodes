﻿using System.Collections.Generic;
using System.Windows.Forms;

/*
 * 源码己托管:https://git.oschina.net/dlgcy/dotnetcodes
 */
namespace DotNet.Utilities.WinFormHelper
{
    public class ControlHelper
    {
        #region CheckedListBox

        /// <summary>
        /// dlgcy:获取CheckedListBox选中项List;
        /// </summary>
        public static List<T> GetCheckedListBoxCheckedList<T>(CheckedListBox ckl)
        {
            List<T> list = new List<T>();
            foreach (var item in ckl.CheckedItems)
            {
                list.Add((T)item);
            }

            return list;
        }

        #endregion
    }
}
