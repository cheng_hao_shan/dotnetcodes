﻿using System;
using System.Drawing;
using System.Windows.Forms;
/*
 * 源码己托管: http://gitee.com/dlgcy/dotnetcodes
 */
namespace DotNet.Utilities.WinFormHelper
{
    /// <summary>
    /// RichTextBox 扩展类
    /// </summary>
    public static class RichTextBoxExtension
    {
        /// <summary>
        /// 附加彩色文本
        /// https://www.cnblogs.com/bobositlife/p/csharp-winform-change-richtextbox-font-color-using-static-extension-method.html
        /// </summary>
        /// <param name="rtBox">RichTextBox对象</param>
        /// <param name="text">文本</param>
        /// <param name="color">颜色</param>
        /// <param name="addNewLine">是否在文本后附加换行</param>
        /// <param name="font">字体</param>
        public static void AppendTextColorful(this RichTextBox rtBox, string text, Color color, bool addNewLine = true, Font font = null)
        {
            if (addNewLine)
            {
                text += Environment.NewLine;
            }

            rtBox.SelectionStart = rtBox.TextLength;
            rtBox.SelectionLength = 0;
            rtBox.SelectionColor = color;
            if (font != null) rtBox.SelectionFont = font;

            rtBox.AppendText(text);
            rtBox.SelectionColor = rtBox.ForeColor;
            if (font != null) rtBox.SelectionFont = rtBox.Font;
        }
    }
}
