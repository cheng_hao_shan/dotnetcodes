﻿using System;
using System.Windows.Forms;
/*
 * 源码己托管: http://gitee.com/dlgcy/dotnetcodes
 */
namespace DotNet.Utilities.WinFormHelper
{
    /// <summary>
    /// Winform 控件调用帮助类
    /// </summary>
    public static class WinformInvokeHelper
    {
        /// <summary>
        /// 控件有与它关联的句柄时，在创建控件的基础句柄所在线程上异步执行指定委托。
        /// </summary>
        /// <param name="control">控件</param>
        /// <param name="method">对不带参数的方法的委托。</param>
        /// <returns>IAsyncResult，无句柄时为 null </returns>
        public static IAsyncResult TryBeginInvoke(this Control control, Delegate method)
        {
            if (control.IsHandleCreated)
            {
                return control.BeginInvoke(method);
            }
            else
            {
                return null;
            }
        }
    }
}
