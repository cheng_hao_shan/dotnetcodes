﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace DotNet.Utilities.WinFormHelper
{
    /// <summary>
    /// Console.Write 重定向到 TextBox
    /// 用法：
    /// 在窗体的构造器里加上下面这句就搞定了.
    /// Console.SetOut(new TextBoxWriter(tbConsole));
    /// tbConsole 是要显示信息的 TextBox 的实例名。
    /// 做完上面的步骤，任何地方调用 Console.write/writeLine, 信息都会在 tbConsole 里打印出来了！
    /// --------------------- 
    /// 作者：wishion 
    /// 原文：https://blog.csdn.net/wangpu8603/article/details/52172931 
    /// </summary>
    public class TextBoxWriter : TextWriter
    {
        readonly TextBox _TextBox;

        delegate void WriteFunc(string value);
        readonly WriteFunc _Write;
        readonly WriteFunc _WriteLine;

        public TextBoxWriter(TextBox textBox)
        {
            this._TextBox = textBox;
            _Write = Write;
            _WriteLine = WriteLine;
        }

        /// <summary>
        /// 使用 UTF-16 避免不必要的编码转换
        /// </summary>
        public override Encoding Encoding => Encoding.Unicode;

        /// <summary>
        /// 最低限度需要重写的方法
        /// </summary>
        /// <param name="value"></param>
        public override void Write(string value)
        {
            if (_TextBox.InvokeRequired)
                _TextBox.BeginInvoke(_Write, value);
            else
                _TextBox.AppendText(value);
        }

        /// <summary>
        /// 为提高效率直接处理一行的输出
        /// </summary>
        /// <param name="value"></param>
        public override void WriteLine(string value)
        {
            if (_TextBox.InvokeRequired)
            {
                _TextBox.BeginInvoke(_WriteLine, value);
            }
            else
            {
                _TextBox.AppendText(value);
                _TextBox.AppendText(this.NewLine);
            }
        }
    }

    /// <summary>
    /// TextBox 扩展类;
    /// </summary>
    public static class TextBoxExtension
    {
        private static int _infoIndex;

        /// <summary>
        /// [dlgcy]在TextBox中追加内容;
        /// </summary>
        /// <param name="tbInfo">需要显示信息的TextBox</param>
        /// <param name="info">需要显示的信息</param>
        /// <param name="showTime">是否打印时间</param>
        public static void ShowInfo(this TextBox tbInfo, string info, bool showTime = true)
        {
            tbInfo.BeginInvoke(new Action(() =>
            {
                tbInfo.AppendText($"[{_infoIndex++}][{DateTime.Now:MM-dd HH:mm:ss}]{info}\r\n\r\n"); 

                // 获取焦点
                tbInfo.Focus();
                // 光标定位到文本最后
                tbInfo.Select(tbInfo.TextLength, 0);
                // 滚动到光标处
                tbInfo.ScrollToCaret();
            }));

            if (_infoIndex > 1000000000)
            {
                _infoIndex = 0;
            }
        }
    }
}
