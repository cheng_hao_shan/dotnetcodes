using System;
using System.Text;

/*
 * 封装操作结果（参考自YT）
 */

namespace DotNet.Utilities
{
    /// <summary>
    /// 封装状态、返回码、消息、异常
    /// </summary>
    public struct Result
    {
        public bool IsSuccess { get; set; }

        public long ResultCode { get; set; }

        public string Message { get; set; }

        public Exception Exception { get; set; }

        public Result(bool success, long resultCode, string error)
        {
            IsSuccess = success;
            ResultCode = resultCode;
            Message = error;
            Exception = null;
        }

        public Result(bool success, long resultCode, string error, Exception exception)
            : this(success, resultCode, error)
        {
            Exception = exception;
        }

        public static implicit operator bool(Result result)
        {
            return result.IsSuccess;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(IsSuccess ? "Success" : "Fail");
            if (!string.IsNullOrEmpty(Message))
                sb.Append(" " + Message);
            return sb.ToString();
        }

        public static Result Success()
        {
            return new Result(true, 0, string.Empty);
        }

        public static Result Fail(string message)
        {
            return new Result(false, 0, message);
        }

        public static Result Fail(long resultCode, string message)
        {
            return new Result(false, resultCode, message);
        }

        public static Result Fail(long resultCode, string message, Exception exception)
        {
            return new Result(false, resultCode, message, exception);
        }

        public static Result Fail(string message, Exception exception)
        {
            return Fail(0, message, exception);
        }
    }

    /// <summary>
    /// 封装状态、返回码、消息、异常、数据
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct Result<T>
    {
        public bool IsSuccess { get; set; }

        public long ResultCode { get; set; }

        public string Message { get; set; }

        public Exception Exception { get; set; }

        public T Value { get; set; }

        public static implicit operator bool(Result<T> result)
        {
            return result.IsSuccess;
        }

        public Result(bool success, long resultCode, string error, T value)
            : this(success, resultCode, error, null, value)
        {
            Value = value;
        }

        public Result(bool success, long resultCode, string error, Exception exception = null, T value = default(T))
        {
            IsSuccess = success;
            ResultCode = resultCode;
            Message = error;
            Exception = exception;
            Value = value;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(IsSuccess ? "Success" : "Fail");
            if (!string.IsNullOrEmpty(Message))
                sb.Append(" " + Message);
            sb.Append($"Value={Value}");
            return sb.ToString();
        }

        public static Result<T> Success(T value)
        {
            return new Result<T>(true, 0, string.Empty, value);
        }

        public static Result<T> Fail(string message)
        {
            return new Result<T>(false, 0, message);
        }

        public static Result<T> Fail(long resultCode, string message)
        {
            return new Result<T>(false, resultCode, message);
        }

        public static Result<T> Fail(long resultCode, string message, Exception exception)
        {
            return new Result<T>(false, resultCode, message, exception);
        }

        public static Result<T> Fail(string message, Exception exception)
        {
            return Fail(0, message, exception);
        }
    }

    /// <summary>
    /// 封装状态、消息
    /// </summary>
    public struct ResultMsg
    {
        public string Msg { get; set; }
        public bool IsSuccess { get; set; }

        public static ResultMsg FailMsg(string msg)
        {
            return new ResultMsg { Msg = msg, IsSuccess = false };
        }
        public static ResultMsg SuccessMsg(string msg = "")
        {
            return new ResultMsg { Msg = msg, IsSuccess = true };
        }
    }

    /// <summary>
    /// 封装状态、消息、数据
    /// </summary>
    public class ResultData
    {
        public string Msg { get; set; }
        public bool IsSuccess { get; set; }
        public object Value { get; set; }

        public static ResultData Fail(string msg = "", object value = null)
        {
            return new ResultData { Value = value, Msg = msg, IsSuccess = false };
        }

        public static ResultData Success(object value = null, string msg = "")
        {
            return new ResultData { Value = value, Msg = msg, IsSuccess = true };
        }

        public static ResultData Warning(object value = null, string msg = "")
        {
            return new ResultData { Value = value, Msg = msg, IsSuccess = true };
        }
    }

    /// <summary>
    /// 封装状态、消息、数据(泛型)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResultData<T>
    {
        public string Msg { get; set; }
        public bool IsSuccess { get; set; }
        public T Value { get; set; }

        public static ResultData<T> Fail(string msg = "")
        {
            return new ResultData<T> { Msg = msg, IsSuccess = false, Value = System.Activator.CreateInstance<T>() };
        }

        public static ResultData<T> Success(T t, string msg = "")
        {
            return new ResultData<T> { Value = t, IsSuccess = true, Msg = msg};
        }
    }
}