﻿using System;
using System.Drawing;

/*
 * 源码己托管: http://gitee.com/dlgcy/dotnetcodes
 */
namespace DotNet.Utilities
{
    /// <summary>
    /// 颜色帮助类;
    /// </summary>
    public class ColorHelper
    {
        /// <summary>
        /// 从 ARGB 字符串转换为颜色
        /// https://www.cnblogs.com/lelehellow/p/6369608.html
        /// </summary>
        /// <param name="colorStr">ARGB颜色字符串（如#FFFFFFFF）</param>
        /// <returns>Color 对象</returns>
        public static Color ConvertFromArgbString(string colorStr)
        {
            if (colorStr.Length != 9 || !colorStr.StartsWith("#"))
            {
                throw new Exception("颜色字符串格式不正确");
            }

            byte[] argb = new byte[4];
            for (int i = 0; i < 4; i++)
            {
                char[] charArray = colorStr.Substring(i * 2 + 1, 2).ToCharArray();
                byte b1 = ToByte(charArray[0]);
                byte b2 = ToByte(charArray[1]);
                argb[i] = (byte)(b2 | (b1 << 4));
            }

            return Color.FromArgb(argb[0], argb[1], argb[2], argb[3]);
        }

        private static byte ToByte(char c)
        {
            byte b = (byte)"0123456789ABCDEF".IndexOf(c);
            return b;
        }

        /// <summary>
        /// 从 RGB 字符串转换为颜色
        /// </summary>
        /// <param name="colorStr">RGB颜色字符串（如#FFFFFF）</param>
        /// <returns>Color 对象</returns>
        public static Color ConvertFromRgbString(string colorStr)
        {
            colorStr = colorStr.Replace("#", "#FF");

            return ConvertFromArgbString(colorStr);
        }
    }
}
