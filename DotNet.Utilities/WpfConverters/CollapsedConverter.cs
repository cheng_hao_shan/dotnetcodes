﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DotNet.Utilities.WpfConverters
{
    public class CollapsedConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string result = "Hidden";

            switch (value)
            {
                case true:
                    result = "Collapsed";
                    break;
                case false:
                    result = "Visible";
                    break;
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class VisibleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string result = "Hidden";

            switch (value)
            {
                case true:
                    result = "Visible";
                    break;
                case false:
                    result = "Collapsed";
                    break;
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
