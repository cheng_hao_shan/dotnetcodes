﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using DLGCY.Utilities;

/*
 * 代码已托管：https://gitee.com/dlgcy/dotnetcodes/tree/dlgcy/DotNet.Utilities/%E6%97%A5%E5%BF%97
 * 依赖：PathHelper 类中删除文件的方法。
 *
 * 修订：2020年7月6日
 */
namespace DotNet.Utilities
{
    /// <summary>
    /// [dlgcy]简易日志类;
    /// </summary>
    public static class LogHelper
    {
        /// <summary>
        /// 前缀;
        /// </summary>
        private static string _Prefix = "";

        /// <summary>
        /// Log 目录（最外层）
        /// </summary>
        private static string _LogDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Log");

        /// <summary>
        /// 日志文件目录（Log 目录 + 前缀）;
        /// </summary>
        private static string LogFileDir => Path.Combine(_LogDir, _Prefix);

        /// <summary>
        /// 文件名;
        /// </summary>
        private static string LogFile => Path.Combine(LogFileDir, $"{(_Prefix == "" ? "Log" : _Prefix)}_{DateTime.Today:yyyyMMdd}.log");

        /// <summary>
        /// 单条内容最大长度;
        /// </summary>
        private static int _MaxLength = 5000;

        /// <summary>
        /// 阻塞集合;
        /// </summary>
        private static readonly BlockingCollection<string> _BC = new BlockingCollection<string>();

        private static readonly Task _Task;

        private static string _lastStr;
        private static int _num;

        static LogHelper()
        {
            if (!Directory.Exists(LogFileDir))
            {
                Directory.CreateDirectory(LogFileDir);
            }

            _Task = new Task(() =>
            {
                foreach (string item in _BC.GetConsumingEnumerable())
                {
                    Start:
                    try
                    {
                        File.AppendAllText(LogFile, item);
                    }
                    catch (DirectoryNotFoundException)
                    {
                        Directory.CreateDirectory(LogFileDir);
                        goto Start;
                    }
                    catch (Exception ex)
                    {
                        //Console.WriteLine($"{ex}");
                        File.AppendAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "LogErr.txt"), $"\r\n{ex}\r\n【日志】{item}");
                    }
                }
            });
        }

        /// <summary>
        /// 配置(注意同一程序中不应多次执行此方法)
        /// </summary>
        /// <param name="prefix">前缀，应用于子目录和文件名</param>
        /// <param name="logDir">Log 目录（最外层），不设置则为"D:\\Log"，设为空则为程序所在目录下的Log目录</param>
        /// <param name="maxLength">单条内容最大长度,留空则为默认值5000</param>
        /// <param name="logReserveDays">删除多少天之前的日志，留空则默认不删除</param>
        public static void Config(string prefix, string logDir = "D:\\Log", int? maxLength = null, int? logReserveDays = null)
        {
            //去除静态构造函数中生成的目录;
            if (Directory.Exists(_LogDir) && _LogDir.StartsWith(AppDomain.CurrentDomain.BaseDirectory) && !string.IsNullOrWhiteSpace(logDir))
            {
                //Directory.Delete(_LogDir);              //删除空目录(非空会报错);
                Directory.Delete(_LogDir, true);  //删除目录和文件;
            }

            if (!string.IsNullOrWhiteSpace(prefix))
            {
                _Prefix = prefix;
            }

            if (!string.IsNullOrWhiteSpace(logDir))
            {
                _LogDir = logDir;
            }

            if (!Directory.Exists(LogFileDir))
            {
                Directory.CreateDirectory(LogFileDir);
            }

            if (maxLength != null && maxLength > 0)
            {
                _MaxLength = (int)maxLength;
            }

            if (logReserveDays != null && logReserveDays > 0)
            {
                System.Timers.Timer timer = new System.Timers.Timer(1) { AutoReset = true, Enabled = true};

                timer.Elapsed += new ElapsedEventHandler((sender, args) =>
                {
                    //如果是第一次执行(刚开始设置的时间间隔为1)
                    if (timer.Interval == 1)
                    {
                        timer.Interval = 1000 * 60 * 60 * 24;
                    }

                    var delDic = PathHelper.DeleteOldFilesInDirAndSubDir(_LogDir, (int)logReserveDays);
                    if (delDic.Count > 0)
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (var pair in delDic)
                        {
                            sb.AppendLine($"删除了目录[{pair.Key}]下的[{pair.Value}]个文件 ");
                        }

                        _BC.Add($"\r\n【简易日志】[{DateTime.Now:HH:mm:ss.ffff}][自动清理]\r\n{sb}\r\n");
                    }
                });
            }
        }

        /// <summary>
        /// 记录日志;
        /// </summary>
        /// <param name="content">日志内容</param>
        /// <param name="isNewLine">前面是否空行</param>
        public static void Write(string content, bool isNewLine = true, [CallerFilePath]string filePath = "", [CallerMemberName]string memberName = "", [CallerLineNumber]int lineNumber = 0)
        {
            if (_Task.Status != TaskStatus.Running && _Task.Status != TaskStatus.WaitingToRun)
            {
                _BC.Add($"\r\n【简易日志】[{DateTime.Now:HH:mm:ss.ffff}][启动日志任务]\r\n");
                _Task.Start();
            }

            int len = content.Length;
            if (len > _MaxLength)
            {
                content = $"{content.Substring(0, _MaxLength / 2)}【...】{content.Substring(len - _MaxLength / 2)}";
            }

            string currentStr = $"{content}{filePath}{memberName}{lineNumber}";
            if (_lastStr == currentStr)
            {
                _BC.Add($"[{DateTime.Now:HH:mm:ss.ffff}][+{++_num}] ");
            }
            else
            {
                _lastStr = currentStr;
                _num = 0;

                //File.AppendAllText(LogFile, $"{(isNewLine?"\r\n":"")}\r\n[{DateTime.Now:HH:mm:ss ffff}][{len}]{content}");
                _BC.Add($"{(isNewLine ? $"\r\n{Path.GetFileNameWithoutExtension(filePath)}/{memberName}/{lineNumber}---->" : "")}\r\n[{DateTime.Now:HH:mm:ss.ffff}][{len}]{content} {(isNewLine ? "\r\n" : "")}");
            }
        }
    }

    /// <summary>
    /// Console.Write 重定向到 日志
    /// 用法：
    /// 在构造器里加上：Console.SetOut(new ConsoleWriter());
    /// </summary>
    public class ConsoleWriter : TextWriter
    {
        /// <summary>
        /// 使用 UTF-16 避免不必要的编码转换
        /// </summary>
        public override Encoding Encoding => Encoding.Unicode;

        /// <summary>
        /// 最低限度需要重写的方法
        /// </summary>
        /// <param name="value"></param>
        public override void Write(string value)
        {
            LogHelper.Write(value, false);
        }

        /// <summary>
        /// 为提高效率直接处理一行的输出
        /// </summary>
        /// <param name="value"></param>
        public override void WriteLine(string value)
        {
            LogHelper.Write(value);
        }
    }
}
