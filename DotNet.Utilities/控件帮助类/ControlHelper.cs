﻿/*
 * 源码己托管:http://gitee.com/dlgcy/dotnetcodes
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace DLGCY.Utilities
{
    /// <summary>
    /// dlgcy:控件帮助类;
    /// </summary>
    public class ControlHelper
    {
        #region CheckBoxList

        /// <summary>
        /// dlgcy:获取CheckBoxList选中项的value值字符串(以逗号分隔);
        /// </summary>      
        public static string GetCheckBoxListSelectedValueStr(CheckBoxList cbl)
        {
            string result = string.Empty;
            foreach (ListItem item in cbl.Items)
            {
                if (item.Selected)
                {
                    if (!string.IsNullOrEmpty(result))
                    {
                        result += ",";
                    }
                    result += item.Value;
                }
            }
            return result;
        }

        /// <summary>
        /// dlgcy:获取CheckBoxList选中项的文本List;
        /// </summary>
        public static List<string> GetCheckBoxListSelectedTextList(CheckBoxList cbl)
        {
            List<string> list = new List<string>();
            foreach (ListItem item in cbl.Items)
            {
                if (item.Selected)
                {
                    list.Add(item.Text.Trim());
                }
            }
            return list;
        }

        /// <summary>
        /// dlgcy:获取CheckBoxList所有项的文本List;
        /// </summary>
        public static List<string> GetCheckBoxListAllTextList(CheckBoxList cbl)
        {
            List<string> list = new List<string>();
            foreach (ListItem item in cbl.Items)
            {
                list.Add(item.Text.Trim());
            }
            return list;
        }

        #endregion

        #region GridView

        /// <summary>
        /// 合并GridView列的单元格
        /// </summary>
        /// <param name="gv">GridView</param>
        /// <param name="col">要进行合并操作的列号</param>
        public static void MergeGvColumnCell(GridView gv, int col)
        {
            if (gv.Rows.Count > 0)
            {
                TableCell oldCell = gv.Rows[0].Cells[col];
                for (int i = 1; i < gv.Rows.Count; i++)
                {
                    TableCell cell = gv.Rows[i].Cells[col];
                    if (oldCell.Text == cell.Text)
                    {
                        cell.Visible = false;
                        if (oldCell.RowSpan == 0)
                        {
                            oldCell.RowSpan = 1;
                        }
                        oldCell.RowSpan++;
                        oldCell.Style["vertical-align"] = "middle !important";
                    }
                    else
                    {
                        oldCell = cell;
                    }
                }
            }
        }

        #endregion

    }
}
