﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DotNet.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Utilities.Tests
{
    [TestClass()]
    public class ConvertHelperTests
    {
        [TestMethod()]
        public void ConvertBaseTest()
        {
            string result = ConvertHelper.ConvertBase("0103007377", 10, 16);
            Assert.IsTrue(result != "0");
        }
    }
}