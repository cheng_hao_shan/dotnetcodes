﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DotNet.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Utilities.Tests
{
    [TestClass()]
    public class QrCodeGmaTests
    {
        [TestMethod()]
        public void BuildQrCodeAsImageTest()
        {
            var image = QrCodeGma.BuildQrCodeAsImage("http://www.lakala.com/");
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "test.jpg");
            bool result = ImageClass.SaveImageToDisk(image, path, ImageFormat.Jpeg);
            Assert.IsTrue(result);
        }
    }
}