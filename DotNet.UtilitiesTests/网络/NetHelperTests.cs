﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DotNet.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Utilities.Tests
{
    [TestClass()]
    public class NetHelperTests
    {
        [TestMethod()]
        public void GetLocalBroadcastIpTest()
        {
            Assert.IsTrue(!string.IsNullOrEmpty(NetHelper.GetLocalBroadcastIp(preferIpPrefix: "192.168.10")));
        }

        [TestMethod()]
        public void CanPingSuccessfullyTest()
        {
            Assert.IsTrue(NetHelper.CanPingSuccessfully("www.baidu.com"));
        }
    }
}