﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DotNet.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Utilities.Tests
{
    [TestClass()]
    public class NetworkManagerTests
    {
        [TestMethod()]
        public void LocalIpAddressTest()
        {
            Assert.IsTrue(!string.IsNullOrEmpty(NetworkManager.LocalIpAddress()));
        }
    }
}