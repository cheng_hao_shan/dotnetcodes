﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DotNet.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Utilities.Tests
{
    [TestClass()]
    public class ConvertJsonTests
    {
        [TestMethod()]
        public void JsonStringFormatTest()
        {
            string str1 = "{\n  \"code\" : 0,\n  \"msg\" : \"请求成功\",\n  \"data\" : [ {\n    \"id\" : 154,\n    \"hotelId\" : 1,\n    \"roomTypeId\" : 31,\n    \"floorId\" : 29,\n    \"roomNumber\" : \"106\",\n    \"featureArr\" : [ \"sunny\", \"east\" ],\n    \"features\" : \"朝阳, 朝东\",\n    \"lock\" : 0,\n    \"statusLabel\" : \"空净\",\n    \"roomType\" : {\n      \"id\" : 31,\n      \"hotelId\" : 1,\n      \"name\" : \"大床房\",\n      \"area\" : \"35\",\n      \"note\" : \"\"\n    },\n    \"floor\" : {\n      \"id\" : 29,\n      \"name\" : \"1楼\"\n    }\n  }, {\n    \"id\" : 155,\n    \"hotelId\" : 1,\n    \"roomTypeId\" : 31,\n    \"floorId\" : 29,\n    \"roomNumber\" : \"107\",\n    \"featureArr\" : [ \"sunny\", \"east\" ],\n    \"features\" : \"朝阳, 朝东\",\n    \"lock\" : 0,\n    \"statusLabel\" : \"空净\",\n    \"roomType\" : {\n      \"id\" : 31,\n      \"hotelId\" : 1,\n      \"name\" : \"大床房\",\n      \"area\" : \"35\",\n      \"note\" : \"\"\n    },\n    \"floor\" : {\n      \"id\" : 29,\n      \"name\" : \"1楼\"\n    }\n  }, {\n    \"id\" : 156,\n    \"hotelId\" : 1,\n    \"roomTypeId\" : 31,\n    \"floorId\" : 29,\n    \"roomNumber\" : \"108\",\n    \"featureArr\" : [ \"sunny\", \"east\" ],\n    \"features\" : \"朝阳, 朝东\",\n    \"lock\" : 0,\n    \"statusLabel\" : \"空净\",\n    \"roomType\" : {\n      \"id\" : 31,\n      \"hotelId\" : 1,\n      \"name\" : \"大床房\",\n      \"area\" : \"35\",\n      \"note\" : \"\"\n    },\n    \"floor\" : {\n      \"id\" : 29,\n      \"name\" : \"1楼\"\n    }\n  }, {\n    \"id\" : 157,\n    \"hotelId\" : 1,\n    \"roomTypeId\" : 31,\n    \"floorId\" : 29,\n    \"roomNumber\" : \"109\",\n    \"featureArr\" : [ \"sunny\", \"east\" ],\n    \"features\" : \"朝阳, 朝东\",\n    \"lock\" : 0,\n    \"statusLabel\" : \"空净\",\n    \"roomType\" : {\n      \"id\" : 31,\n      \"hotelId\" : 1,\n      \"name\" : \"大床房\",\n      \"area\" : \"35\",\n      \"note\" : \"\"\n    },\n    \"floor\" : {\n      \"id\" : 29,\n      \"name\" : \"1楼\"\n    }\n  }, {\n    \"id\" : 158,\n    \"hotelId\" : 1,\n    \"roomTypeId\" : 31,\n    \"floorId\" : 29,\n    \"roomNumber\" : \"1010\",\n    \"featureArr\" : [ \"sunny\", \"east\" ],\n    \"features\" : \"朝阳, 朝东\",\n    \"lock\" : 0,\n    \"statusLabel\" : \"空净\",\n    \"roomType\" : {\n      \"id\" : 31,\n      \"hotelId\" : 1,\n      \"name\" : \"大床房\",\n      \"area\" : \"35\",\n      \"note\" : \"\"\n    },\n    \"floor\" : {\n      \"id\" : 29,\n      \"name\" : \"1楼\"\n    }\n  }, {\n    \"id\" : 275,\n    \"hotelId\" : 1,\n    \"roomTypeId\" : 31,\n    \"floorId\" : 44,\n    \"roomNumber\" : \"901\",\n    \"featureArr\" : [ \"sunny\", \"scenery\" ],\n    \"features\" : \"朝阳, 景观房\",\n    \"lock\" : 0,\n    \"statusLabel\" : \"空净\",\n    \"roomType\" : {\n      \"id\" : 31,\n      \"hotelId\" : 1,\n      \"name\" : \"大床房\",\n      \"area\" : \"35\",\n      \"note\" : \"\"\n    },\n    \"floor\" : {\n      \"id\" : 44,\n      \"name\" : \"9楼\"\n    }\n  } ]\n}";

            //Assert.IsTrue(!string.IsNullOrEmpty(ConvertJson.JsonStringFormat(str1, true)));
            //Assert.IsTrue(!string.IsNullOrEmpty(ConvertJson.JsonStringFormat(str1, false)));
            Assert.IsTrue(!string.IsNullOrEmpty(ConvertJson.JsonStringFormat(str1, false, false)));
        }
    }
}