﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DotNet.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Utilities.Tests
{
    [TestClass()]
    public class PathHelperTests
    {
        [TestMethod()]
        public void DeleteOldFilesInSubDirTest()
        {
            //var delDic = PathHelper.DeleteOldFilesInSubDir("D:\\Log", 20, SearchOption.AllDirectories);
            var delDic = PathHelper.DeleteOldFilesInSubDir("D:\\Log", 17);
            Assert.IsTrue(delDic.Values.Sum() > 0);
        }

        [TestMethod()]
        public void DeleteOldFilesInDirAndSubDirTest()
        {
            var delDic = PathHelper.DeleteOldFilesInDirAndSubDir("D:\\Log", 16);
            Assert.IsTrue(delDic.Values.Sum() > 0);
        }
    }
}