﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DotNet.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace DotNet.Utilities.Tests
{
    [TestClass()]
    public class ColorHelperTests
    {
        [TestMethod()]
        public void ConvertFromRgbStringTest()
        {
            Color color = ColorHelper.ConvertFromRgbString("#742F1C");
            Assert.IsTrue(color != null);
        }
    }
}