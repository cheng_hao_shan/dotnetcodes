﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DotNet.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Utilities.Tests
{
    [TestClass()]
    public class EnumExtensionTests
    {
        /// <summary>
        /// 枚举类型
        /// </summary>
        enum ELanguage
        {
            CSharp = 0,
            Java = 1,
            VB = 2
        }

        /// <summary>
        /// 枚举对象
        /// </summary>
        private ELanguage _eLanguage = ELanguage.CSharp;

        [TestMethod()]
        public void GetEnumListTest1()
        {
            List<ELanguage> list = _eLanguage.GetEnumList(); //通过枚举对象获取枚举列表;
            Assert.IsTrue(list.Count > 0);
        }

        [TestMethod()]
        public void GetEnumListTest()
        {
            List<ELanguage> list = EnumExtension.GetEnumList<ELanguage>(); //通过枚举类型获取枚举列表;
            Assert.IsTrue(list.Count > 0);
        }

        [TestMethod()]
        public void GetAllItemsTest()
        {
            var allItems = _eLanguage.GetAllItems<ELanguage>(); //通过枚举对象获取所有枚举;
            List<ELanguage> list = allItems.ToList();
            Assert.IsTrue(list.Count > 0);
        }

        [TestMethod()]
        public void GetAllItemsTest1()
        {
            var allItems = EnumExtension.GetAllItems<ELanguage>(); //通过枚举类型获取所有枚举;
            List <ELanguage> list = allItems.ToList();
            Assert.IsTrue(list.Count > 0);
        }
    }
}