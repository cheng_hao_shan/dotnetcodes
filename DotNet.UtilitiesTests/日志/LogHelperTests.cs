﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DotNet.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DotNet.Utilities.Tests
{
    [TestClass()]
    public class LogHelperTests
    {
        [TestMethod()]
        public void WriteTest()
        {
            LogHelper.Write("11111111111111");
            LogHelper.Write("22222222222222");
            Assert.IsTrue(true);
        }

        /// <summary>
        /// 测试日志配置
        /// </summary>
        [TestMethod()]
        public void WriteTest1()
        {
            LogHelper.Config("前缀");
            LogHelper.Write("33333333333333");
            LogHelper.Write("44444444444444");
            Assert.IsTrue(true);
        }

        /// <summary>
        /// 测试日志重复时简化为计数
        /// </summary>
        [TestMethod()]
        public void WriteTest2()
        {
            for (int i = 0; i < 88; i++)
            {
                LogHelper.Write("55555555555");
            }

            for (int i = 0; i < 99; i++)
            {
                Thread.Sleep(1);

                LogHelper.Write("66666666666");
            }

            for (int i = 0; i < 100; i++)
            {
                LogHelper.Write("77777777777");
            }

            Thread.Sleep(1000);

            Assert.IsTrue(true);
        }

        /// <summary>
        /// 测试日志配置(自动删除)
        /// </summary>
        [TestMethod()]
        public void WriteTest3()
        {
            LogHelper.Config("Test", logReserveDays:10);
            LogHelper.Write("88888888888888");
            Assert.IsTrue(true);
        }
    }
}