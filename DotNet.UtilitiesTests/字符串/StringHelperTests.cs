﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DotNet.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Utilities.Tests
{
    [TestClass()]
    public class StringHelperTests
    {
        [TestMethod()]
        public void GetCnEnSubStringRemoveMiddleTest()
        {
            string str = "hello World".GetCnEnSubStringRemoveMiddle(5);
            str = "hello 世界".GetCnEnSubStringRemoveMiddle(5);
            Assert.IsTrue(!string.IsNullOrEmpty(str));
        }

        [TestMethod()]
        public void ListToStrTest()
        {
            List<string> list = new List<string>() { "111", "222", "333", "444" };
            string str = list.ListToStr(",", "''");
            Assert.IsTrue(!string.IsNullOrEmpty(str));
        }

        [TestMethod()]
        public void ListToStrTest1()
        {
            List<string> list = new List<string>() { "111", "222", "333", "444" };
            string str = list.ListToStr(",");
            Assert.IsTrue(!string.IsNullOrEmpty(str));
        }

        [TestMethod()]
        public void ListToStrTest2()
        {
            List<string> list = new List<string>() { "111", "222", "333", "444" };
            string str = StringHelper.ListToStr(list, ",", "'", true);
            Assert.IsTrue(!string.IsNullOrEmpty(str));
        }

        [TestMethod()]
        public void GetStrEndNumAddOneTest()
        {
            string str = "1A0-001";
            str = str.GetStrEndNumAddOne();

            str = "A99";
            str = StringHelper.GetStrEndNumAddOne(str);

            str = "A56";
            str = str.GetStrEndNumAddOne();

            Assert.IsTrue(true);
        }
    }
}