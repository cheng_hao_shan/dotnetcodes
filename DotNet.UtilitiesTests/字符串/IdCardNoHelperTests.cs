﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DotNet.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Utilities.Tests
{
    [TestClass()]
    public class IdCardNoHelperTests
    {
        [TestMethod()]
        public void GetIdCardInfoTest()
        {
            Assert.IsTrue(IdCardNoHelper.GetIdCardInfo("412825198012127419")[4] == "1");
        }
    }
}